# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:


let
  home-manager = builtins.fetchTarball "https://github.com/nix-community/home-manager/archive/master.tar.gz";
in

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      (import "${home-manager}/nixos")
    ];

  # BOOT LOADER
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";
  boot.kernelPackages = pkgs.linuxPackages_latest;
  
  # SWAP WITH HIBERNATION 
  
  # boot.resumeDevice = "/dev/disk/by-uuid/efb2c8a0-169b-4d4f-b09f-f656f617a49e";
  # boot.kernelParams = [ "resume_offset=21317632" ];
  # powerManagement.enable = true;
  
  # HOSTNAME
  networking.hostName = "FEBNIX"; 
  
  # NETWORK MANAGER
  networking.networkmanager.enable = true;
  # programs.nm-applet.enable = true;
  # network-manager-applet.enable = true;
    
  # Network Proxy
  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";
  
  # TIME ZONE
  time.timeZone = "Asia/kolkata";

  # LOCALE 
  i18n.defaultLocale = "en_US.UTF-8";
  
  # X11-X-ORG
  services.xserver.enable = true;
  services.xserver.xautolock.enable = true;
  
  # WINDOW AND LOGIN MANAGERS 
  # XFCE
  services.xserver.displayManager.lightdm.enable = true;
  services.xserver.desktopManager.xfce.enable = true;


  # Window Managers
  # services.xserver.windowManager.awesome.enable = true;
  services.xserver.windowManager.dwm.enable = true;
    
  # KDE-Plasma
  #services.xserver.displayManager.sddm.enable = true;
  #services.xserver.desktopManager.plasma5.enable = true;
    
  # PRINTING
  #services.printing.enable = true;
  #programs.system-config-printer.enable = true;
  #services.printing.drivers = [ pkgs.gutenprint pkgs.gutenprintBin pkgs.hplip pkgs.hplipWithPlugin pkgs.samsung-unified-linux-driver pkgs.splix ];

  # SCANNING
  #hardware.sane.enable = true;
  #hardware.sane.extraBackends = [ pkgs.hplipWithPlugin ];
  
  # BLUETOOTH
  # hardware.bluetooth.enable = true;
  # services.blueman.enable = true;
    
  # SYSTEM SOUND
  # PipeWire
  # rtkit is optional but recommended
  #security.rtkit.enable = true;
  #services.pipewire = {
  #enable = true;
  #alsa.enable = true;
  #alsa.support32Bit = true;
  #pulse.enable = true;
  # If you want to use JACK applications, uncomment this
  #jack.enable = true; 
#};

  # Pulseaudio
  hardware.pulseaudio.enable = true;
    
  # GVFS
  services.gvfs.enable = true;
   
  # VIRTUALISATION
  #virtualisation.libvirtd.enable = true;
  #programs.dconf.enable = true; 
  
  # ADB
  #programs.adb.enable = true;
  
  # FLATPACK STORE
  #services.flatpak.enable = true;
  #xdg.portal.enable = true;
  #xdg.portal.extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
  
  # SYNCTHING (Device File Sync)
  # services.syncthing.enable = true;
  
  # Neovim
  # programs.neovim.enable = true;
  
  # qt5
  #qt5ct.platformTheme = "qt5ct";
  #programs.qt5ct.enable = true;
  
  # SHELLS
  
  # Fish Shell 
  programs.fish.enable = true;
  users.users.febnix.shell = pkgs.fish;
  #users.defaultUserShell = pkgs.fish;
  
  #Zsh Shell
  #programs.zsh.enable = true;
  #users.defaultUserShell = pkgs.zsh;
  #programs.zsh.ohMyZsh.enable = true;
  #programs.zsh-autosuggestions.enable = true;
  
  # KEYMAP
  services.xserver = {
  layout = "us";
  xkbVariant = "";
  };
 # gnome-keyring = { enable = true; };
 
 # Enable touchpad support (enabled default in most desktopManager).
 # services.xserver.libinput.enable = true;

 # USER ACCOUNTS
   users.users.febnix = {
   isNormalUser = true;
   description = "FEB-NIX";
   extraGroups = [ "networkmanager" "wheel" "audio" "scanner" "lp" "libvirtd" "video" "camera" "lp" "kvm" "plex" ];
   #packages = with pkgs; [
   #pasystray 
   #cbatticon
   #];
};

# HOME MANAGER 

 home-manager.users.febnix = {
 home.stateVersion = "22.05";
 home.packages = with pkgs; [
 vim ];
 };
   
 # SECURITY
 # security.rtkit.enable = true;
 
 # Environment Variables
 environment.variables = {
    QT_QPA_PLATFORMTHEME = "qt5ct";
 
  };
  
  # FONTS 
  fonts.fonts = with pkgs; [                # Fonts
    carlito                                 # NixOS
    vegur                                   # NixOS
    source-code-pro
    jetbrains-mono
    font-awesome                            # Icons
    corefonts                               # MS
    ];

 console = {
    font = "Lat2-Terminus16";
    # keyMap = "us";	                    # or us/azerty/etcK
  };

  
 # SYSTEM PACKAGES
     
 environment.systemPackages = with pkgs; [
 # Terminal ----------
 # alacritty
   terminator
 # starship
 
 # Theams ----------
 arc-theme
 arc-icon-theme
 plata-theme
 bibata-cursors
 bibata-extra-cursors
 capitaine-cursors
 papirus-icon-theme
 hicolor-icon-theme
 # material-design-icons
 # material-icons
 # nerdfonts
 # paper-gtk-theme
 
 # Internet ----------
 # brave
 firefox
 # youtube-dl
 # qbittorrent
 # ungoogled-chromium
 
 # Mail and Messengers
 # tdesktop
 # thunderbird-bin
 
 # Utilities ----------
 # volctl
 # bitwarden
 # conky
 # ulauncher
 # flameshot
  p7zip unzip zip
  xarchiver
  gnome.file-roller
  picom
 # git
 # wget
 # curl
 # neovim 
 # lm_sensors 
 pfetch 
 # vim
 # piper

 # Awesome WM Utils 
 # polybar
 
 # Audiuo and Video ----------
 
 # kdenlive
 # obs-studio
 # blender 
 # simplescreenrecorder
 # vlc
 # nuclear
 # mpv
 # pavucontrol
 # tenacity
 # celluloid
 # spotify
 
 # Desktop Applications ----------
 
 # libreoffice
 # onlyoffice-bin
 # gnome3.gnome-calculator
 # gnome.simple-scan
 # gimp
 # krita
 # okular
 # parcellite
 # joplin-desktop
 # gnome.gucharmap
 # shotwell
  
 
 # Stock Trading ----------
 # zeroad

 # File and Folders ----------
 ripgrep 
 pcmanfm
 mlocate
 rofi     
 pkgs.nextcloud25 

 # Virtualisation ----------
 # virt-manager

 # XFCE Plugins-----
 # xfce.ristretto 
 xfce.xfce4-whiskermenu-plugin
 xfce.xfce4-panel-profiles 
 
];

 # UPDATE AND SERVICES ----------

 nix.settings.auto-optimise-store = true;
 nixpkgs.config.allowUnfree = true;        # Allow proprietary software.
 
	
  
  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  networking.firewall.enable = true;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?
 

}

